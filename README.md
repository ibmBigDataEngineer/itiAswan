# [IBM Skills Academy](https://ibm.biz/skillsacademy)
Course Material : https://ibm.biz/BdsxzV

# Send your problem by email if you can not create a new issue
Send any question or problem you have to "incoming+ibmbigdataengineer-itiaswan-14211833-issue-@incoming.gitlab.com"

### Install IBM Cloud CLI ibmBigDataEngineer/itiAswan#6
### Upload files to HDFS using `hdfs dfs` command ibmBigDataEngineer/itiAswan#9
# Instructors:
Ramy Said: ramys@eg.ibm.com <a href="https://www.linkedin.com/in/ramy-said-25ba96ba/"><img src="https://simpleicons.org/icons/linkedin.svg" width="12" height="12"></a>

Mohamed Hassan: noureldin@ibm.com <a href="https://www.linkedin.com/in/mmnoureldin/"><img src="https://simpleicons.org/icons/linkedin.svg" width="12" height="12"></a>


# Exams second attempts trials:
Asmaa Mohamed: asmaa@eg.ibm.com


# Important websites you need to have a good profile on 
* www.kaggle.com
* https://www.kaggle.com/c/house-prices-advanced-regression-techniques
* www.youracclaim.com
* www.github.com



# Important websites for courses 
* www.coursera.org (Andrew NG Stanford University Machine Learning) 
* www.cognitiveclass.ai
* www.datacamp.com
* https://www.facebook.com/groups/big.data.egypt/
* IBM Watson Studio on the cloud : https://datascience.ibm.com
* http://ibm.onthehub.com/
